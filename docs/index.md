# Welcome to Mr Love: Queen's Choice Bot
I made this short website with examples.

## Help Command
* help - Help Menu `help`
    * Example 
        ![help](img/help.png)
    * Help Menu's Categorizes `help (Categorizes)`
        ![help1](img/help1.png)

Now we can move on to each categorize and their commands!

## BotInfo
* creator - Who made the bot? `creator`
    * Example:
        ![creator](img/creator.png)
* suggest - Suggest something and I will send my creator a Direct Message of your suggestion! (Please don't abuse it too much) `suggest (Type something here)`
    * Example:
        ![suggest](img/suggest.png)

        My side of view:
        ![creator](img/suggestdm.png)

## UserInfo
* avatar - Returns members avatar.
    * Example of your own avatar: `avatar`
        ![youravatar](img/youravatar.png)

    * Example of other's avatar: `avatar (mention user)`
        ![othersavatar](img/othersavatar.png)

## Expert
* expert - Show Expert Info `expert (expert name)`
    * Example:
        ![expert](img/expert.png)

## Normal
* normal - Show Normal Info Chapter 1-22 `normal (Chapter)-(Stage)`
    * Example `normal 1-1`
        ![normal1](img/normal1.png)

    * Example `normal 1-2`
        ![normal2](img/normal2.png)

    * Chapter 19 Guy's Stage Example: `normal Victor-1`
        ![ch19guystage](img/ch19guystage.png)

    * Dream Event Example `normal Dream-1`
        ![dreamevent](img/dreamevent.png)

## Elite
* elite - Show Elite Info Chapter 1-18 10 stages each chapter `elite (Chapter)-(Stage)`
    * Example:
        ![elite](img/elite.png)

## Footage
* footage - Show Footages Info `footage (Guy's Name) (Chapter)-(Stage)`
    * Example `footage Lucien 2-2`
        ![footage](img/footage.png)

## HeartTrials
* dheart - Show Decision Heart Trial Stage that you ask. `dheart (Guy's Name) (Chapter)-(Stage)`
    * Example `dheart Victor 1-1`
        ![dheart](img/dheart.png)

* cheart - Show Creativity Heart Trial Stage that you ask. `cheart (Guy's Name) (Chapter)-(Stage)`
    * Example `cheart Lucien 1-1`
        ![cheart](img/cheart.png)

* aheart - Show Affinity Heart Trial Stage that you ask. `aheart (Guy's Name) (Chapter)-(Stage)`
    * Example `aheart Kiro 1-1`
        ![aheart](img/aheart.png)

* eheart - Show Execution Heart Trial Stage that you ask. `eheart (Guy's Name) (Chapter)-(Stage)`
    * Example `eheart Gavin 1-1`
        ![eheart](img/eheart.png)

* hearttime - Show Heart Trial Dates `hearttime`
    * Example:
        ![hearttime](img/hearttime.png)

## Karma
* kssr - SSR EXP to Max & Evolve Level `kssr`
    * Example:
        ![kssr](img/kssr.png)

* ksr - SR EXP to Max & Evolve Level `ksr`
    * Example:
        ![ksr](img/ksr.png)

* kr - R EXP to Max & Evolve Level `kr`
    * Example:
        ![kr](img/kr.png)

* karmaf - Show Karma Info (no image/all in one) `karmaf (Guy's Name) (Karma Card Name)`
    * Example:
        ![karmaf](img/karmaf.png)

* karma - Show Karma Info (no image/seperate evolve info) `karma (Guy's Name) (Karma Card Name)`
    * Example:
        ![karma](img/karma.gif)

* karmai - Show Karma Info (with image) `karmai (Guy's Name) (Karma Card Name)`
    * Example:
        ![karmai](img/karmai.gif)